class Student

attr_accessor :first_name, :last_name, :courses
def initialize(first_name, last)
  @first_name = first_name
  @last_name = last
  @courses = []
end

def name
  "#{@first_name} #{@last_name}"
end

def enroll(course)
  raise Exception if has_conflict(course)
  if !course.students.include?(self) 
     @courses.push(course)
     course.students.push(self)
  end
end

def has_conflict(second_course)
  @courses.each do |course|
    return true if course.conflicts_with?(second_course)
  end
  return false
end

def course_load
  hash = Hash.new(0)
  @courses.each do |course|
    hash[course.department] += course.credits
  end
  hash
end


end
